Addictive Points : An easy to install loyalty program for your website.

Create a partner account at:-

http://www.addictivepoints.com/partner

And start rewarding your site visitors.

This module makes it easy to install the Addictive Points platform onto your
site and incentivize people to interact. Visitors get points for visiting your
site, liking your site on Facebook and following you on Twitter.

You can also upgrade to a Flexible package and choose to reward your visitors
for any action you create; filling out a survey, watching a video or uploading
content to your site. You choose the actions, you choose the rewards; you reward
the engagement.

Steps to get started:-

1 : Navigate to http://www.addictivepoints.com/partner.
2 : Fill in the details for your site.
3 : Follow the simple four step setup process.
4 : Note down the Addictive Points API key; it's needed at module activation.
5 : Install the module (see below).
6 : Navigate to the administration page and paste in your API key.
7 : Select where you want the Addictive Points tab to appear on your site.
8 : Click 'save configuration'; you're set to start rewarding your visitors.

NOTE : There may be a small delay while your account is activated. Once your
account is activated, you will be able to access more information about your
visitors from your Addictive Points Dashboard. You will also be able to get
access to your API key.

Installation
------------

Copy the addictivepoints folder from the archive to the modules directory in
your Drupal installation and enable  it on the admin modules page. In the module
configuration page you can enter your Addictive Points key and set where you
want the Addictive Points tab to appear on your site.

Author
------
Tytn Hays
Tytn@AddictivePoints.Com
