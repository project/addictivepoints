Drupal.behaviors.addictivepoints = function (context) {
  var apoints_api_key = Drupal.settings.addictivepoints.api_key;
  var tab_x = Drupal.settings.addictivepoints.tab_x;
  var tab_y = Drupal.settings.addictivepoints.tab_y;

  var _apapi = _apapi || {};
  _apapi.settings = {
          key:"'" + apoints_api_key + "'",
          position: {x:"'" + tab_x + "'", y:"'" + tab_y + "'"}};

  (function(){
    var apoints = document.createElement("script");
    apoints.type = "text/javascript"; apoints.async = true;
    apoints.src =
         ("https:" == document.location.protocol ? "https://" : "http://")
 + "www.addictivepoints.com/api/js/addictive-points.js";
    var s = document.getElementsByTagName("script")[0];
    s.parentNode.insertBefore(apoints, s);})();
}
